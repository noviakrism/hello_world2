import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  List<String> products;
  Products(this.products);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: products
          .map(
            (el) => Container(
              margin: EdgeInsets.all(9.0),
              child: Card(
                elevation: 9.0,
                  child: Column(
                    children: <Widget>[
                    Container(child: Image.asset('assets/food.jpg'),
                    margin: EdgeInsets.all(10.0),),
                      Container(
                        child:
                      Text(el), 
                      margin: EdgeInsets.only(bottom: 9.0),
                      ),
                    ],
                  ),
                ),
            )
          )
          .toList(),
    );
  }
}
